/* Теоретичні питання
1. Яке призначення методу event.preventDefault() у JavaScript?
2. В чому сенс прийому делегування подій?
3. Які ви знаєте основні події документу та вікна браузера? 

Практичне завдання:

Реалізувати перемикання вкладок (таби) на чистому Javascript.

Технічні вимоги:

- У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
- Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

 Умови:
 - При реалізації обов'язково використовуйте прийом делегування подій (на весь скрипт обробник подій повинен бути один).
*/





/*
1. Метод event.preventDefault() у JavaScript використовується для того, 
щоб відмінити дію, яку зазвичай виконує браузер у відповідь на певну подію. 

2. Делегування подій у JavaScript - це механізм, за допомогою якого один обробник подій встановлюється на батьківський елемент,
 і він реагує на події, які виникають на його дочірніх елементах. 

3. Основні події документу та вікна браузера включають:

Події документу (Document Events):
    1.DOMContentLoaded
    2.load
    3.beforeunload
    4.unload 

Події вікна браузера (Window Events):
    1.resize
    2.scroll
    3.focus
    4.blur
    5.load
*/

const btnContainer = document.querySelector(".centered-content");

btnContainer.addEventListener("click", (event) => {

  const tabsContainer = document.getElementById("tabs-container");

  if (!event.target.id.endsWith("-btn")) {
    // If not, return early without further processing
    return;
  }

  const expectedSuffix = "-btn";

  let activeButton = document.querySelector(".active");
  let activeContent = document.querySelector(".active-content");


  if (activeButton) {
    activeButton.classList.remove("active");
  }

  if (activeContent) {
    activeContent.classList.remove("active-content");
  }

  if (event.target.id.endsWith(expectedSuffix)) {
    event.target.classList.add("active");

    let baseId = event.target.id.slice(0, -expectedSuffix.length);
    let relatedContentId = `${baseId}-related`;

    let relatedContent = document.getElementById(relatedContentId);

    if (relatedContent) {
      relatedContent.classList.add("active-content");
    } 

    const allContent = document.querySelectorAll(".content");
    allContent.forEach(content => {
      if (content.id !== relatedContentId) {
        content.classList.remove("active-content");
      }
    });
  }


});
